import React, { Component } from 'react'
import ApolloClient from 'apollo-boost'
import { ApolloProvider } from 'react-apollo'

// import components
import Navbar from './components/Navbar'

// set up apollo client
const client = new ApolloClient({
	uri: 'http://localhost:3000/graphql'
})

class App extends Component {
  render() {
    return (
    	<ApolloProvider client={client}>
      	<div>
      		
      	</div>
      </ApolloProvider>
    )
  }
}

export default App
