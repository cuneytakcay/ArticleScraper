const express = require('express')
const graphqlHTTP = require('express-graphql')
const mongoose = require('mongoose')

// import modules
const schema = require('./schema/schema')
const dbURI = require('./private/private')

const app = express()
const PORT = process.env.PORT || 3000

// connect to mLab database
mongoose.connect(dbURI.db)
mongoose.connection.once('open', () => {
	console.log('Connection to articlescraper_db in mLab is successful')
})

app.use('/graphql', graphqlHTTP({
	schema,
	graphiql: true,
}))

app.listen(PORT, () => {
	console.log('Listening for requests on port', PORT)
})

