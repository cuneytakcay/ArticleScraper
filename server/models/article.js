const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ArticleSchema = new Schema({
	title: {
		type: String,
		unique: true,
	},
	summary: {
		type: String,
	},
})

module.exports = mongoose.model('Article', ArticleSchema)