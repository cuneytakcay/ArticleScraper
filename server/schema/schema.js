const graphql = require('graphql')
const Article = require('../models/article')
const Note = require('../models/note')

// get graphql properties
const {
	GraphQLObjectType,
	GraphQLString,
	GraphQLSchema,
	GraphQLID,
	GraphQLList,
	GraphQLNonNull,
} = graphql

const ArticleType = new GraphQLObjectType({
	name: 'Article',
	fields: () => ({
		id: { type: GraphQLID },
		title: { type: GraphQLString },
		summary: { type: GraphQLString },
		notes: {
			type: new GraphQLList(ArticleType),
			resolve(parent, args) {
				return Note.find({ articleId: parent.id })
			}
		}
	}),
})

const NoteType = new GraphQLObjectType({
	name: 'Note',
	fields: () => ({
		id: { type: GraphQLID },
		title: { type: GraphQLString },
		text: { type: GraphQLString },
		article: {
			type: ArticleType,
			resolve(parent, args) {
				return Article.findById(parent.articleId)
			}
		},
	}),
})

const RootQuery = new GraphQLObjectType({
	name: 'RootQueryType',
	fields: {
		article: {
			type: ArticleType,
			args: { id: { type: GraphQLID } },
			resolve(parent, args) {
				return Article.findById(args.id)
			}
		},
		note: {
			type: NoteType,
			args: { id: { type: GraphQLID } },
			resolve(parent, args) {
				return Note.find(args.id)
			}
		},
		articles: {
			type: new GraphQLList(ArticleType),
			resolve(parent, args) {
				return Article.find({})
			}
		},
		notes: {
			type: new GraphQLList(NoteType),
			resolve(parent,args) {
				return Note.find({})
			}
		}
	},
})

const Mutation = new GraphQLObjectType({
	name: 'Mutation',
	fields: {
		addArticle: {
			type: ArticleType,
			args: {
				title: { type: new GraphQLNonNull(GraphQLString) },
				summary: { type: new GraphQLNonNull(GraphQLString) },
			},
			resolve(parent, args) {
				let article = new Article({
					title: args.title,
					summary: args.summary,
				})

				return article.save()
			},
		},
		addNote: {
			type: NoteType,
			args: {
				title: { type: new GraphQLNonNull(GraphQLString) },
				text: { type: new GraphQLNonNull(GraphQLString) },
				articleId: { type: new GraphQLNonNull(GraphQLID) },
			},
			resolve(parent, args) {
				let note = new Note({
					title: args.title,
					text: args.text,
					articleId: args.articleId,
				})

				return note.save()
			},
		},
	},
})

module.exports = new GraphQLSchema({ 
	query: RootQuery,
	mutation: Mutation, 
})